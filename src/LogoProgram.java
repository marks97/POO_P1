/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta classe conté la funció main del programa. No te paràmetres d'entrada ni cap valor de
 * retorn. És on afegim les instruccions que la classe programa emmagatzemarà i, posteriorment i,
 * si són correctes, seran impreses per pantalla. Si aquestes no són correctes, les instruccions
 * no s'imprimiran, sinó que s'imprimiran els errors en el seu lloc.
 *
*/

public class LogoProgram {

    public static void main(String[] args) {


        //Creem un nou programa, i li afegim instruccions

        Program p = new Program("TWO_SQUARE");

        p.addInstruction("REP" , 2);
        p.addInstruction("PEN", 1);
        p.addInstruction("REP" , 4);
        p.addInstruction("FWD",100);
        p.addInstruction("ROT",90);
        p.addInstruction("END" , 1);
        p.addInstruction("PEN",0);
        p.addInstruction("FWD", 300);
        p.addInstruction("END" , 1);

        //Comprobem que el programa estigui correcte. Si no ho és imprimim els errors per pantalla

        if (p.isCorrect()) {

            //Iterem totes les instruccions, i les imprimim si són vàlides
            while (!p.hasFinished()) {
                Instruction x = p.getNextInstruction();
                if(x!=null)
                    System.out.println(x.info());
            }
        } else
            p.printErrors();

        p.restart();

    }

}