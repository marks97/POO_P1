/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Program. Com a paràmetre del constructor rebem  el nom del programa.
 * Aquí principalment és on emmagatzemarem les instruccions, i si n'hi ha, errors.
 *
 * Com a variables tenim tres llistes, en les quals desarem o bé instruccions, o bé errors.
 * L'altre llista ens servirà com a auxiliar en una funció de la classe.
 *
 * També tenim la variable que contindrà el nom del propi programa, anomenada 'programName' ,
 * i la variable currentLine , de tipo enter, que ens servirà també alhora de imprimir les
 * instruccions.
 *
*/

import javafx.util.Pair;

import java.util.LinkedList;

public class Program {

    private LinkedList<Instruction> instructions;
    private LinkedList<Instruction> errors;
    private LinkedList<Pair<Integer, Double>> repInstructions;

    private int currentLine;
    private String programName;

    //Aquí tenim el constructor de la classe.

    public Program(String name){
        programName = name;
        currentLine = 0;
        instructions = new LinkedList<>();
        errors = new LinkedList<>();
        repInstructions = new LinkedList<>();

    }

    public String getName() {
        return programName;
    }

    //Aquesta funció serveix per afegir instruccions a la llista enllaçada 'instruccions' .

    public boolean addInstruction(String code, double param){

        Instruction instruction = new Instruction (code,param);

        //Comprobem que la instrucció és correcta. Si ho és l'afegim a la llista 'instruccions' ,
        //sinó, l'afegirem a la llista d'errors.

        if (instruction.isCorrect()){
            instructions.add(instruction);
            return true;

        } else {
            errors.add(instruction);
            return false;
        }
    }

    //Aquesta funció reinicia a 0 totes les variables 'contenidores'

    public void restart() {
        currentLine = 0;
        errors.clear();
        instructions.clear();
        repInstructions.clear();
    }

    //Aquesta funció comproba que la instrucció que anem a evaluar no sigui l'última.

    public boolean hasFinished() {
        return
                currentLine == instructions.size();
    }

    //Aquesta funció ens serveix per iterar la llista de instruccions, tenint en compte
    // els bucles de repeticions.

    public Instruction getNextInstruction() {

        Instruction instruction = instructions.get(currentLine);

        if(instruction.isRepInstruction()) { //Comprobem que la instrucció és de tipus REP o tipus END

            if(instruction.getCode().equals("REP")) {
                //Si la instrucció és de tipus REP, desem a la llista 'repInstructions'
                //la linia de la instrucció actual REP, i el seu valor (nombre de repeticions).
                currentLine++;
                repInstructions.add(new Pair<>(currentLine, instruction.getParam()));
                return null;
            }

            else {

                //Si la instrucció es de tipus END, obtenim l'instrucció desada a l'última línia REP desada
                //a la llista repInstructions.

                Instruction repInstruction = instructions.get(
                        repInstructions.getLast().getKey()
                );

                //Aquest és el nombre de repeticions del valor de l'última instrucció REP desada.
                double param = repInstructions.getLast().getValue();

                //Si tant sols queda una repetició, eliminarem l'últim valor del vector repInstructions

                if(param == 1) {
                    currentLine++;
                    repInstructions.removeLast();
                    return null;
                } else {

                    //Asignem a la currentLine el valor el qual era la currentLine en el moment en el que vam trobar una
                    //instrucció tipo REP
                    currentLine = repInstructions.getLast().getKey();

                    //Si queda més d'una repetició, en restarem una
                    repInstructions.set(
                            repInstructions.size() - 1, new Pair<>(currentLine, param-1)
                    );

                    currentLine++;

                    return repInstruction;
                }
            }

        } else {
            //Si la instrucció no és de tipus END ni de tipus REP, la retornarem perquè sigui impresa.
            currentLine++;
            return instruction;
        }

    }

    //Aquesta funció imprimirà per pantalla tots els error desats en la llista 'errors' , i també el tipus
    //d'error que és.

    public void printErrors(){

        for (Instruction i : errors) {

                System.out.println("error:  " + i.info() + "\n");

                //En funció del codi d'error simprimeix un missatge o un altre.

                if (i.errorCode() == 1){
                    System.out.println("El codi està malament, recorda que els únic codis que pots posar són: REP. FWD, PEN, ROT i END");
                } else if (i.errorCode() == 2){
                    System.out.println("EL parámetre està malament, en aquest cas només pots posar valors entre (-1000,1000)");
                } else if (i.errorCode() == 3){
                    System.out.println("El paràmtre està malament, en aquest cas només pots posar els valors 0 o 1");
                } else if (i.errorCode() == 4){
                    System.out.println("El paràmtre està malament, en aquest cas només pots posar valors entre (-360, 360)");
                } else if (i.errorCode() == 5){
                    System.out.println("El paràmtre està malament, en aquest cas només pots posar valors entre (0, 100)");
                }

                System.out.println();
        }

    }

    //Aquesta funció comproba que hi hagi el mateix nombre de instruccions REP que de tipus END, ja que així
    //ens assegurem que no hi ha un bloc que no ha estat tencat.

    public boolean hasSameEndRep() {

        int reps = 0;
        int ends = 0;

        for(Instruction i : instructions) {

            String s = i.getCode();

            if (s.equals("REP"))
                reps++;
            else if(s.equals("END"))
                ends++;
        }

        //Si no hi ha el mateix nombre imprimirem el missatge d'error per pantalla.

        if(reps!=ends)
            System.out.println("El programa està malament. Hi ha un dels blocs REP - END que no ha set tencat.");

        return reps == ends;

    }

    //Aquesta funció comproba que el programa no te cap error.

    public boolean isCorrect(){
        return
                errors.size() == 0 && hasSameEndRep();
    }
}
